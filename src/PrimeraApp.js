
import React, { Fragment } from 'react';
import PropTypes from "prop-types";

const PrimeraApp = ({ saludo, subtitulo }) => {
    // const saludo = {
    //     HOLA: 'SDSD'
    // };
    return (
        // <><pre>{JSON.stringify(saludo, null, 3)}</pre>
        <><h1>{saludo}</h1>
            <p>{subtitulo}</p></>
    );

}
PrimeraApp.propTypes = {
    saludo: PropTypes.string.isRequired,
    subtitulo: PropTypes.string,
    
}

PrimeraApp.defaultProps={
    subtitulo: "Maria Vera"
}

export default PrimeraApp;